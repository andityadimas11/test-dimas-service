package main

import (
	"database/sql"
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/lib/pq"
)

const (
	numRecords     = 1000
	recordsPerTask = 100
)

type Record struct {
	ID        int
	Customer  string
	Price     float64
	Quantity  int
	Timestamp string
}

func main() {
	connStr := "user=andityadimas dbname=postgres sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	var records []Record
	for i := 1; i <= numRecords; i++ {
		record := Record{
			ID:        i,
			Customer:  "dimas",
			Price:     100.00,
			Quantity:  1,
			Timestamp: "2023-01-01",
		}
		records = append(records, record)
	}

	startTime := time.Now()

	var wg sync.WaitGroup
	taskCount := (numRecords + recordsPerTask - 1) / recordsPerTask

	for i := 0; i < taskCount; i++ {
		wg.Add(1)
		go func(startIdx int) {
			defer wg.Done()
			endIdx := startIdx + recordsPerTask
			if endIdx > numRecords {
				endIdx = numRecords
			}
			recordsToProcess := records[startIdx:endIdx]
			err := processAndInsertRecords(recordsToProcess, db)
			if err != nil {
				log.Printf("Error processing records: %v", err)
			}
		}(i * recordsPerTask)
	}

	wg.Wait()

	elapsedTime := time.Since(startTime)
	fmt.Printf("Processed %d records in %s\n", numRecords, elapsedTime)
}

func processAndInsertRecords(records []Record, db *sql.DB) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	stmt, err := tx.Prepare(pq.CopyIn("service", "id", "customer", "price", "quantity", "timestamp"))
	if err != nil {
		return err
	}
	defer stmt.Close()

	for _, record := range records {
		_, err := stmt.Exec(record.ID, record.Customer, record.Price, record.Quantity, record.Timestamp)
		if err != nil {
			return err
		}
	}

	_, err = stmt.Exec()
	if err != nil {
		return err
	}

	err = stmt.Close()
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}
